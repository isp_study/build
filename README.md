# Docker образ для сборки проекта

Данный образ предназначен для компиляции проектов с использованием conan. Установлен clang 7 и conan последней версии. Основа - Ubuntu 18.10. Запущен ssh сервер. Пользователь для работы: conan.

# Запуск

Вам необходим установленный docker. Сначала логинимся в докер репозитарий. Вводим логин и пароль от gitlab.com:
```
docker login https://registry.gitlab.com/v2/isp_study
```
Дальше запускаем контейнер. Т.к. контейнера не существует локально он будет скачан на ваш компьютер:
```
docker run -d -p2202:22 -p8080:8080 --privileged --name build registry.gitlab.com/isp_study/build 
```
Запущенный контейнер можно увидеть в списке docker контейнеров.
```
docker ps
```
Скачиваем приватный ssh ключ из данного репозитория (с помощью него мы будем попадать в контейнер). Не забудьте выставить права на этот файл:
```
git clone https://gitlab.com/isp_study/build.git
cd build
chmod 600 ssh/id_rsa
```
Теперь в контейнер можно войти по ssh:
```
ssh -i ssh/id_rsa conan@localhost -p 2202
```
Проверяем, что действительно попали в  контейнер с Ubuntu.
```
uname -a
```
Выходим.
```
exit
```
Гасим контейнер
```
docker stop build
```

# CLion

Скачайте триальную версию CLion, проверялось на версии 2019.1. https://www.jetbrains.com/clion/download/

Настройте **File > Settings > Build, Execution, Deployment > Toolchains**. Добавьте новый.

Name: local docker

Смените System на Remote Host

Credentials: нажмите на папку, появятся настройки подключения:
```
Host: localhost
Port: 2202
User name: conan
Autentification type: Key pair
Private key file: <путь куда вы клонировали проект build>/ssh/id_rsa
```
Сохраните настройки.

Добавьте проект (если проект открыт, пропустите этот шаг): **VCS > Checkout from Version Control > Git** Url: https://gitlab.com/NAMESPACE/meeting_backend
Где NAMESPACE ваш login в gitlab в рамках которого вы Форкнули проект.

Выберите файл CMakeList.txt, нажмите **Load CMake project**.

Вы должны в окне CMake увидеть ошибку:
```
include could not find load file:
   /tmp/tmp.JONdWhD1cl/cmake-build-debug/conanbuildinfo.cmake
```
Скопируйте путь до *conanbuildinfo.cmake*, в моем случае это */tmp/tmp.JONdWhD1cl/cmake-build-debug*. Подключитесь к терминалу:
**Tools > Start SSH session > local docker**.

В появившейся консоли введите:
```
cd /tmp/tmp.JONdWhD1cl/cmake-build-debug
conan install ../ --build=missing
```
После того как закончится сборка библиотек, нажмите **Tools > CMake > Reload CMake project**. После завершения этой операции вы можете скомпилировать поект: **Build > Build project**.

Чтобы работало перемещение по заголовочным файлам библиотек нажмите **Toold > Resync with Remote hosts**.



