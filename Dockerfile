FROM conanio/clang7
LABEL maintainer "dimakirk@gmail.com"
RUN mkdir /home/conan/.ssh && sed -i -e '1s/^/export PYENV_ROOT=\/opt\/pyenv\n/' /home/conan/.bashrc && \
 conan profile new default && conan profile update settings.compiler=clang default && \
 conan profile update settings.compiler.version=7.0 default &&  conan profile update settings.compiler.libcxx=libc++ default && \
 conan profile update settings.build_type=Debug default && conan profile update settings.os=Linux default && \
 conan profile update settings.arch=x86_64 default
COPY ssh/id_rsa.pub /home/conan/.ssh/authorized_keys
USER root
RUN apt-get update && apt-get install -y openssh-server gdbserver gdb vim rsync clang-tidy && mkdir /root/.ssh
EXPOSE 22
EXPOSE 2202
EXPOSE 8080
COPY docker-entrypoint.sh /bin/
RUN chmod +x /bin/docker-entrypoint.sh && mkdir /run/sshd && chown conan /home/conan/.ssh/authorized_keys && chgrp 1001 /home/conan/.ssh/authorized_keys && chmod 600 /home/conan/.ssh/authorized_keys

USER conan
RUN echo "[requires]\nPoco/1.9.0@pocoproject/stable\n[options]\nPoco:shared=True" > /home/conan/conanfile.txt && \
    conan install /home/conan --build=missing && \
    rm -fr /home/conan/conanfile.txt
USER root
ENTRYPOINT ["docker-entrypoint.sh"]
